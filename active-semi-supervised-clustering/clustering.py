from sklearn import datasets, metrics
from active_semi_clustering.semi_supervised.pairwise_constraints import PCKMeans
from active_semi_clustering.active.pairwise_constraints import ExampleOracle, ExploreConsolidate, MinMax
from dataset_xy import get_labeled_data, get_unlabeled_data
import numpy as np
import csv
def clustering_to_file(labeled_data_file,unlabeled_data_file) :
    print("here")
    X, y = get_labeled_data(labeled_data_file)
    J=get_unlabeled_data(unlabeled_data_file)
    H=X.copy()
    X=np.array(X)
    y=np.array(y)
    H.extend(J)
    H=np.array(H)


    oracle = ExampleOracle(y, max_queries_cnt=10)

    active_learner = MinMax(n_clusters=2)
    active_learner.fit(X, oracle=oracle)
    pairwise_constraints = active_learner.pairwise_constraints_

    clusterer = PCKMeans(n_clusters=2)
    clusterer.fit(X, ml=pairwise_constraints[0], cl=pairwise_constraints[1])
    #here the main code

    f=ExploreConsolidate(n_clusters=2)
    f.fit(X,oracle)
    pairwise_constraints1 = f.pairwise_constraints_
    clusterer1 = PCKMeans(n_clusters=2)
    clusterer1.fit(H, ml=pairwise_constraints1[0], cl=pairwise_constraints1[1])

    metrics.adjusted_rand_score(y, clusterer.labels_)

    label=clusterer1.labels_.tolist()


    with open('clustered-data/clustered_data.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["GPA","FATHER EMPLOY","MOTHER EMPLOY","parent marriage status","self estamate","discipline","dropout"])
        i=0
        for item in H :
            row=item
            row=row.tolist()
            row=row.copy()
            if label[i]==1 :
                row.append('yes')
            else :
                row.append('no')
            i+=1
            writer.writerow(row)


