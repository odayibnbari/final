
'''
data must be after after  processing in these formate
GPA,FATHER EMPLOY,MOTHER EMPLOY,parent marriage status,self estamate,discipline,dropout
60,yes,no,divorce,5,8,yes


'''

def get_labeled_data(file):
    with open(file, 'r') as fin:
        lines = fin.readlines()
        # remove '\n' characters
        clean_lines = [l.strip('\n') for l in lines]
        # split on tab so that we get lists from strings
        A = [cl.split('\t') for cl in clean_lines]
        # get lists of ints instead of lists of strings
        X = []
        Y = []
        flag = False
        for rown in A:
            if flag:
                row = rown[0].split(",")
                temp = []
                for i in range(6):
                    if i == 0 or i == 4 or i == 5:
                        temp.append(int(row[i]))
                    if i == 1 or i == 2:
                        if row[i] == 'yes':
                            temp.append(1)
                        else:
                            temp.append(0)
                    if i == 3:
                        if row[i] == 'marriage':
                            temp.append(1)
                        else:
                            temp.append(0)

                if row[6] == 'yes':
                    Y.append(1)
                else:
                    Y.append(0)
                X.append(temp)
            else:
                flag = True

        return (X,Y)
def get_unlabeled_data(file):
    with open(file, 'r') as fin:
        lines = fin.readlines()
        # remove '\n' characters
        clean_lines = [l.strip('\n') for l in lines]
        # split on tab so that we get lists from strings
        A = [cl.split('\t') for cl in clean_lines]
        # get lists of ints instead of lists of strings
        X = []
        flag = False
        for rown in A:
            if flag:
                row = rown[0].split(",")
                temp = []
                for i in range(6):
                    if i == 0 or i == 4 or i == 5:
                        temp.append(int(row[i]))
                    if i == 1 or i == 2:
                        if row[i] == 'yes':
                            temp.append(1)
                        else:
                            temp.append(0)
                    if i == 3:
                        if row[i] == 'marriage':
                            temp.append(1)
                        else:
                            temp.append(0)
                X.append(temp)
            else:
                flag = True

        return X
