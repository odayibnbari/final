import mysql.connector
import wx
import pandas as pd
import numpy

from GUI.Panels.AddPupilPanel import AddPupilPanel
from GUI.Panels.DeveloperPanel import DeveloperPanel
from GUI.Panels.LoginPanel import LoginPanel
from GUI.Panels.PupilsPanel import PupilsPanel
from GUI.Panels.RolePanel import RolePanel
from GUI.Panels.SchoolPanel import SchoolPanel



class MainFrame(wx.Frame):
    def __init__(self):
        super().__init__(parent=None,style=wx.MINIMIZE_BOX |wx.CLOSE_BOX , title='Dropout Predictive')
        self.mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="Veve831674",
            database="dropoutDatabase"
        )
        self.enter=False;
        self.mycursor = self.mydb.cursor()
        self.Centre()
        self.InitUI()
       # self.SetBackgroundColour(wx.Colour(0x90caf9))
        self.labels = numpy.array([])
        self.sizer = wx.BoxSizer()
        self.SetSizer(self.sizer)
        self.set_panel_login()
        self.set_panel_main_menu()
        self.panel_menu.Hide()
        self.set_panel_school()
        self.panel_school.Hide()
        self.set_panel_add_Pupil()
        self.panel_add_Pupil.Hide()
        self.set_panel_pupils_det()
        self.panel_pupil_det.Hide()
        self.set_panel_developer()
        self.panel_developer.Hide()

        self.SetClientSize(self.panel_one.GetSize() + (100, 10))

        self.Show()
        self.Centre()

    def set_panel_login(self):
        self.panel_one = LoginPanel(self)
        self.sizer.Add(self.panel_one, 1, wx.EXPAND)
        self.panel_one.my_btn.Bind(wx.EVT_BUTTON, self.show_panel_main_menu)



    def checkSchoolCode(self):
        sql = "INSERT INTO" + "schoolcode7" + " (name, code) VALUES (%s, %s)"
        val = ("exschool", "a1b2c3")
        '''self.mycursor.execute(sql, val)
        self.mydb.commit()
        self.mycursor.execute("SELECT * FROM schoolcode7")
        myresult = self.mycursor.fetchall()
        schoolcode=str(self.panel_one.text_ctrl.GetValue())
        flag=False
        for code in myresult:
            if code[1] == schoolcode :
                flag=True
        print(flag)
        print(schoolcode)'''
        schoolcode = str(self.panel_one.text_ctrl.GetValue())

        if schoolcode =='a1b2c3':
            self.enter= True
        else:
            self.enter= False






    def show_panel_login(self, event):
        self.hideAll()
        self.set_panel_login()
        self.panel_one.Show()
        self.SetClientSize(self.panel_one.GetSize() + (100, 10))
        self.Layout()
        self.Centre()

    def set_panel_school(self):
        self.panel_school=SchoolPanel(self)
        self.sizer.Add(self.panel_school, 1, wx.EXPAND)
        self.panel_school.back_btn.Bind(wx.EVT_BUTTON, self.show_panel_main_menu)
        self.panel_school.add_btn.Bind(wx.EVT_BUTTON, self.show_panel_add_Pupil)
        self.panel_school.show_btn.Bind(wx.EVT_BUTTON, self.show_panel_pupils_det)


    def show_panel_school(self, event):
        self.hideAll()
        self.set_panel_school()
        self.panel_school.Show()
        self.SetClientSize(self.panel_school.GetSize() + (100, 10))
        self.Layout()
        self.Centre()

    def set_panel_add_Pupil(self):
        self.panel_add_Pupil = AddPupilPanel(self)
        self.sizer.Add(self.panel_add_Pupil, 1, wx.EXPAND)
        self.panel_add_Pupil.button5.Bind(wx.EVT_BUTTON, self.show_panel_school)
    def show_panel_add_Pupil(self,event):
        self.hideAll()
        self.set_panel_add_Pupil()
        self.panel_add_Pupil.Show()
        self.SetClientSize(self.panel_add_Pupil.GetSize() + (100, 10))
        self.Layout()
        self.Centre()
    def set_panel_pupils_det(self):
        self.panel_pupil_det=PupilsPanel(self)
        self.sizer.Add(self.panel_pupil_det,1,wx.EXPAND)
        self.panel_pupil_det.closebut.Bind(wx.EVT_BUTTON, self.show_panel_school)


    def show_panel_pupils_det(self,event):
        self.hideAll()
        self.set_panel_pupils_det()
        self.panel_pupil_det.Show()
        self.SetClientSize((1030,800) )
        self.Layout()
        self.Centre()



    def set_panel_main_menu(self):
        self.panel_menu = RolePanel(self)
        self.sizer.Add(self.panel_menu, 1, wx.EXPAND)
        self.panel_menu.back_btn.Bind(wx.EVT_BUTTON, self.show_panel_login)
        self.panel_menu.dev_btn.Bind(wx.EVT_BUTTON, self.show_panel_developer)
        self.panel_menu.sch_btn.Bind(wx.EVT_BUTTON, self.show_panel_school)

    def show_panel_main_menu(self, event):
        self.checkSchoolCode()
        if self.enter :
            self.hideAll()
            self.set_panel_main_menu()
            self.panel_menu.Show()
            self.SetClientSize(self.panel_menu.GetSize() + (100, 10))
            self.Layout()
            self.Centre()
        else:
            self.ShowMessage()


    def ShowMessage(self):
        wx.MessageBox('Invalid Password', 'Error',
                      wx.OK )
    def set_panel_developer(self):
        self.panel_developer = DeveloperPanel(self)
        self.sizer.Add(self.panel_developer, 1, wx.EXPAND)
        self.panel_developer.btn.Bind(wx.EVT_BUTTON, self.show_panel_main_menu)
        #self.panel_menu.dev_btn.Bind(wx.EVT_BUTTON, self.show_panel_menu)
        #self.panel_menu.sch_btn.Bind(wx.EVT_BUTTON, self.show_panel_school)

    def show_panel_developer(self, event):
        self.hideAll()
        self.set_panel_developer()
        self.panel_developer.Show()
        self.SetClientSize(self.panel_developer.GetSize() + (100, 10))
        self.Layout()
        self.Centre()

    def hideAll(self):
        self.sizer.Detach(self.panel_menu)
        self.sizer.Detach(self.panel_school)
        self.sizer.Detach(self.panel_one)
        self.sizer.Detach(self.panel_add_Pupil)
        self.sizer.Detach(self.panel_pupil_det)
        self.sizer.Detach(self.panel_developer)
        self.panel_developer.Hide()
        self.panel_school.Hide()
        self.panel_menu.Hide()
        self.panel_one.Hide()
        self.panel_pupil_det.Hide()
        self.panel_add_Pupil.Hide()

    def train_model(self,event):
        '''self.kmeans = Kmeans(3)
        xl = pd.ExcelFile("ExampleData.xlsx")
        df = xl.parse("Sheet1")
        self.X = numpy.array(df)
        for i in range(self.X.shape[0]):
            if (self.X[i, 7] == 'Not-working'):
                self.X[i, 7] = 1
            if (self.X[i, 7] == 'Died'):
                self.X[i, 7] = 2
            if (self.X[i, 7] == 'Part-time'):
                self.X[i, 7] = 3
            if (self.X[i, 7] == 'Full-time'):
                self.X[i, 7] = 4
            if (self.X[i, 8] == 'Not-working'):
                self.X[i, 8] = 1
            if (self.X[i, 8] == 'Died'):
                self.X[i, 8] = 2
            if (self.X[i, 8] == 'Part-time'):
                self.X[i, 8] = 3
            if (self.X[i, 8] == 'Full-time'):
                self.X[i, 8] = 4

        self.kmeans.train(self.X)
        self.labels = self.kmeans.getLabels()'''

    def InitUI(self):
        menubar = wx.MenuBar()
        fileMenu = wx.Menu()
        qmi = wx.MenuItem(fileMenu, 1, '&Quit\tCtrl+Q')

        fileMenu.Append(qmi)
        self.Bind(wx.EVT_MENU, self.OnQuit, id=1)
        menubar.Append(fileMenu, '&File')
        self.SetMenuBar(menubar)
        self.Centre()

    def OnQuit(self, e):
        self.Close()



        '''panel = wx.Panel(self)
        my_sizer = wx.BoxSizer(wx.VERTICAL)
        label = wx.StaticText(panel, label="Dropout Predictive")
        stylish_font = wx.Font(wx.FontInfo(20).FaceName("liberation").Bold())
        label.SetFont(stylish_font)
        #label.SetForegroundColour(wx.Colour(0x00FF44))
        my_sizer.Add(label, 0, wx.ALL | wx.CENTER, 5);
        image = wx.Bitmap("imgs/cap-2.png")
        d=wx.StaticBitmap(panel, wx.ID_ANY, image)
        my_sizer.Add(d, 0, wx.ALL | wx.CENTER, 5);
        label = wx.StaticText(panel, label="")
        my_sizer.Add(label, 0, wx.ALL | wx.CENTER, 5);
        label = wx.StaticText(panel, label="Enter school code")
        my_sizer.Add(label, 0, wx.ALL | wx.CENTER, 5);
        self.text_ctrl = wx.TextCtrl(panel)
        my_sizer.Add(self.text_ctrl, 0, wx.ALL | wx.CENTER, 5)
        my_btn = wx.Button(panel, label='Confirm')
        my_sizer.Add(my_btn, 0, wx.ALL | wx.CENTER, 5)
        panel.SetSizer(my_sizer)
        my_sizer.Fit(self)
        '''
        #panel.Fit()