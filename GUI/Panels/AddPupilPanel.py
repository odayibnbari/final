import wx
import mysql.connector
class AddPupilPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.frame=parent
        self.mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="Veve831674",
            database="dropoutDatabase"
        )
        self.mycursor = self.mydb.cursor()

        sizer = wx.GridBagSizer(5, 4)
        text1 = wx.StaticText(self, label="Pupil")
        stylish_font = wx.Font(wx.FontInfo(20).FaceName("liberation").Bold())
        text1.SetFont(stylish_font)
        sizer.Add(text1, pos=(0, 0), flag=wx.TOP | wx.LEFT | wx.BOTTOM,
                  border=15)
        icon = wx.StaticBitmap(self, bitmap=self.scale_bitmap(wx.Bitmap('imgs/backpack.png'),50,50))
        sizer.Add(icon, pos=(0, 3), flag=wx.TOP | wx.RIGHT | wx.ALIGN_RIGHT,
                  border=5)

        line = wx.StaticLine(self)
        sizer.Add(line, pos=(1, 0), span=(1, 5),
                  flag=wx.EXPAND | wx.BOTTOM, border=10)

        text2 = wx.StaticText(self, label="Name")
        sizer.Add(text2, pos=(2, 0), flag=wx.LEFT, border=10)

        self.tc1 = wx.TextCtrl(self)
        sizer.Add(self.tc1, pos=(2, 1), span=(1, 3), flag=wx.TOP | wx.EXPAND)

        text3 = wx.StaticText(self, label="ID")
        sizer.Add(text3, pos=(3, 0), flag=wx.LEFT, border=10)

        self.tc2 = wx.TextCtrl(self)
        sizer.Add(self.tc2, pos=(3, 1), span=(1, 3), flag=wx.TOP | wx.EXPAND)

        text4 = wx.StaticText(self, label="GPA")
        sizer.Add(text4, pos=(4, 0), flag=wx.LEFT, border=10)
        self.tc3 = wx.TextCtrl(self)
        sizer.Add(self.tc3, pos=(4, 1), span=(1, 3), flag=wx.TOP | wx.EXPAND)

        text7 = wx.StaticText(self, label="Parents Status")
        sizer.Add(text7, pos=(5, 0), flag=wx.LEFT, border=10)
        statuses2 = ['Marriage', 'Divorce']
        self.combo2 = wx.ComboBox(self, choices=statuses2)
        sizer.Add(self.combo2, pos=(5, 1), span=(1, 3), flag=wx.TOP | wx.EXPAND)

        text5 = wx.StaticText(self, label="Father Status")
        sizer.Add(text5, pos=(6, 0), flag=wx.LEFT, border=10)
        statuses = ['Working', 'Not-working', 'Died']
        self.combo = wx.ComboBox(self, choices=statuses)
        sizer.Add(self.combo, pos=(6, 1), span=(1, 3),flag=wx.TOP | wx.EXPAND)

        text6 = wx.StaticText(self, label="Mother Status")
        sizer.Add(text6, pos=(7, 0), flag=wx.LEFT, border=10)
        statuses1 = ['Working', 'Not-working', 'Died']
        self.combo1 = wx.ComboBox(self, choices=statuses1)
        sizer.Add(self.combo1, pos=(7, 1), span=(1, 3),flag=wx.TOP | wx.EXPAND)

        text8 = wx.StaticText(self, label="Self Esteem")
        sizer.Add(text8, pos=(8, 0), flag=wx.LEFT, border=10)
        statuses3 = ['1','2','3','4','5','6','7','8','9','10']
        self.combo3 = wx.ComboBox(self, choices=statuses3)
        sizer.Add(self.combo3, pos=(8, 1), span=(1, 3), flag=wx.TOP | wx.EXPAND)

        text9 = wx.StaticText(self, label="Discipline")
        sizer.Add(text9, pos=(9, 0), flag=wx.LEFT, border=10)
        statuses4 = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
        self.combo4 = wx.ComboBox(self, choices=statuses4)
        sizer.Add(self.combo4, pos=(9, 1), span=(1, 3), flag=wx.TOP | wx.EXPAND)

        my_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.button4 = wx.Button(self, label="Add")
        self.button4.Bind(wx.EVT_BUTTON, self.add_Pubil)
        my_sizer.Add(self.button4, 0, wx.ALL | wx.CENTER, 5)
        #sizer.Add(button4, pos=(10, 2))
        self.button5 = wx.Button(self, label="Cancel")
        my_sizer.Add(self.button5, 0, wx.ALL | wx.CENTER, 5)

        sizer.Add(my_sizer, pos=(10, 3), span=(1, 1), flag=wx.BOTTOM | wx.RIGHT, border=0)
        #sizer.Add(button5, pos=(10, 3), span=(1, 1),flag=wx.BOTTOM | wx.RIGHT, border=10)



        sizer.AddGrowableCol(2)

        self.SetSizer(sizer)
        sizer.Fit(self)
    def add_Pubil(self,event):
        name=self.tc1.GetValue()
        id=self.tc2.GetValue()
        gpa=self.tc3.GetValue()
        parentStatus=self.combo2.GetValue()
        fatherStatus=self.combo.GetValue()
        motherStatus = self.combo1.GetValue()
        selfesteem=self.combo3.GetValue()
        discipline=self.combo4.GetValue()

        sql = "INSERT INTO SchoolPupils (name, gpa,id,partnersStatus,fatherStatus,motherStatus,selfEsteem,discipline) VALUES (%s, %s,%s,%s,%s,%s,%s,%s)"
        val =(name,gpa,id,parentStatus,fatherStatus,motherStatus,selfesteem,discipline)
        self.mycursor.execute(sql, val)
        self.mydb.commit()
        print(self.mycursor.rowcount, "record inserted.")
        self.empty_cells()
    def empty_cells(self):
        self.ShowMessage()
        self.tc1.SetValue("")
        self.tc2.SetValue("")
        self.tc3.SetValue("")
        self.combo2.SetValue("")
        self.combo.SetValue("")
        self.combo1.SetValue("")
        self.combo3.SetValue("")
        self.combo4.SetValue("")

    def ShowMessage(self):
        wx.MessageBox('successfully added to the system', 'Done',
                      wx.OK )

    def scale_bitmap(self,bitmap, width, height):
        image = wx.ImageFromBitmap(bitmap)
        image = image.Scale(width, height, wx.IMAGE_QUALITY_HIGH)
        result = wx.BitmapFromImage(image)
        return result