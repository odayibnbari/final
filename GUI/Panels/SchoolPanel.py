import wx
class SchoolPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.frame=parent
        my_sizer = wx.BoxSizer(wx.VERTICAL)
        label = wx.StaticText(self, label=" School ")
        stylish_font = wx.Font(wx.FontInfo(20).FaceName("liberation").Bold())
        label.SetFont(stylish_font)
        # label.SetForegroundColour(wx.Colour(0x00FF44))
        my_sizer.Add(label, 0, wx.ALL | wx.CENTER, 5)
        image = wx.Bitmap("imgs/school.png")
        d = wx.StaticBitmap(self, wx.ID_ANY, image)
        my_sizer.Add(d, 0, wx.ALL | wx.CENTER, 5)
        label = wx.StaticText(self, label="")
        my_sizer.Add(label, 0, wx.ALL | wx.CENTER, 5)

        self.add_btn = wx.Button(self, label='  Add pupil ')
        my_sizer.Add(self.add_btn, 0, wx.ALL | wx.EXPAND , 5)
        self.show_btn = wx.Button(self, label='  Show pupils status  ')
        my_sizer.Add(self.show_btn, 0, wx.ALL | wx.EXPAND, 5)

        self.back_btn = wx.Button(self, 0, label="Back")
        my_sizer.Add(self.back_btn, 0, wx.ALL | wx.CENTER, 5)
        my_sizer.SetSizeHints(self)
        self.SetSizer(my_sizer)
        my_sizer.Fit(self)
