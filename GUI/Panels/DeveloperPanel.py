import wx

from clustering import clustering_to_file


class DeveloperPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.frame=parent
        self.labeled_path="/Users/odayibnbari/Desktop/לימודים/פרויקט גמר/final project/active-semi-supervised-clustering/data.csv"
        self.unlabeled_path="/Users/odayibnbari/Desktop/לימודים/פרויקט גמר/final project/active-semi-supervised-clustering/data.csv"
        self.my_sizer = wx.BoxSizer(wx.VERTICAL)
        label = wx.StaticText(self, label="Data Clustering")
        stylish_font = wx.Font(wx.FontInfo(20).FaceName("liberation").Bold())
        label.SetFont(stylish_font)
        # label.SetForegroundColour(wx.Colour(0x00FF44))
        self.my_sizer.Add(label, 0, wx.ALL | wx.CENTER, 5)
        image = wx.Bitmap("imgs/cluster.png")
        d = wx.StaticBitmap(self, wx.ID_ANY, image)
        self.my_sizer.Add(d, 0, wx.ALL | wx.CENTER, 5)

        sb1 = wx.StaticBox(self, label="")
        self.data_sizer = wx.StaticBoxSizer(sb1, wx.VERTICAL)


        label1 = wx.StaticText(self, label="Upload unlabeled data")
        stylish_font = wx.Font(wx.FontInfo(20).FaceName("liberation").Bold())
        label1.SetFont(stylish_font)
        self.data_sizer.Add(label1, flag=wx.CENTER, border=5)
        label = wx.StaticText(self, label="")
        self.data_sizer.Add(label, flag=wx.CENTER, border=5)
        # label.SetForegroundColour(wx.Colour(0x00FF44))
        image = wx.Bitmap("imgs/xls.png")
        self.labeled_icon = wx.StaticBitmap(self, wx.ID_ANY, image)
        self.data_sizer.Add(self.labeled_icon, flag=wx.CENTER, border=5)
        self.labeled_icon.Show(False)
        label = wx.StaticText(self, label="")
        self.data_sizer.Add(label, flag=wx.CENTER, border=5)

        self.unlabeled_btn = wx.Button(self, label='  Choose file  ')
        self.data_sizer.Add(self.unlabeled_btn, flag=wx.CENTER, border=5)

        self.unlabeled_btn.Bind(wx.EVT_BUTTON, self.unlabeled_dialog)



        self.my_sizer.Add(self.data_sizer, 0, wx.ALL | wx.CENTER, 5)

        #----

        sb1 = wx.StaticBox(self, label="")
        self.data_sizer1 = wx.StaticBoxSizer(sb1, wx.VERTICAL)

        label1 = wx.StaticText(self, label="   Upload labeled data   ")
        stylish_font = wx.Font(wx.FontInfo(20).FaceName("liberation").Bold())
        label1.SetFont(stylish_font)
        self.data_sizer1.Add(label1, flag=wx.CENTER, border=5)

        label1 = wx.StaticText(self, label="")
        self.data_sizer1.Add(label1, flag=wx.CENTER, border=5)
        # label.SetForegroundColour(wx.Colour(0x00FF44))
        image = wx.Bitmap("imgs/xls.png")
        self.unlabeled_icon = wx.StaticBitmap(self, wx.ID_ANY, image)
        self.data_sizer1.Add(self.unlabeled_icon, flag=wx.CENTER, border=5)
        self.unlabeled_icon.Show(False)
        # label.SetForegroundColour(wx.Colour(0x00FF44))

        label = wx.StaticText(self, label="")
        self.data_sizer1.Add(label, flag=wx.CENTER, border=5)

        self.labeled_btn = wx.Button(self, label='  Choose file  ')
        self.data_sizer1.Add(self.labeled_btn, flag=wx.CENTER, border=5)

        self.labeled_btn.Bind(wx.EVT_BUTTON, self.labeled_dialog)

        self.my_sizer.Add(self.data_sizer1, 0, wx.ALL | wx.CENTER, 5)

        self.btn_cluster = wx.Button(self, 0, label="Cluster Data")
        self.btn_cluster.Bind(wx.EVT_BUTTON, self.cluster_data)
        self.my_sizer.Add(self.btn_cluster, 0, wx.ALL | wx.CENTER, 5)
        self.btn_cluster.Show(False)

        self.btn = wx.Button(self, 0, label="Back")
        self.my_sizer.Add(self.btn, 0, wx.ALL | wx.CENTER, 5)

        self.SetSizer(self.my_sizer)
        self.my_sizer.Fit(self)


    def unlabeled_dialog(self,event):
        openFileDialog = wx.FileDialog(self.frame, "Open", "","","*.csv",wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        openFileDialog.ShowModal()
        self.ShowMessage()
        print(openFileDialog.GetPath())
        self.unlabeled_path=openFileDialog.GetPath()
        self.labeled_icon.Show(True)

        if self.unlabeled_icon.IsShown():
            self.btn_cluster.Show(True)
        self.frame.SetClientSize(self.GetSize() + (10, 40))
        openFileDialog.Destroy()
    def labeled_dialog(self,event):
        openFileDialog = wx.FileDialog(self.frame, "Open", "", "",
                                       "*.csv",
                                       wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        openFileDialog.ShowModal()

        print(openFileDialog.GetPath())
        self.ShowMessage()
        self.labeled_path=openFileDialog.GetPath()
        self.unlabeled_icon.Show(True)
        openFileDialog.Destroy()
        if self.labeled_icon.IsShown():
            self.btn_cluster.Show(True)
        self.frame.SetClientSize(self.GetSize() + (10, 40))
    def cluster_data(self,event):
        clustering_to_file(self.labeled_path, self.unlabeled_path)
    def ShowMessage(self):
        wx.MessageBox('The file has been uploaded successfully', 'Completed',
                      wx.OK )




