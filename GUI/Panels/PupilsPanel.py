import csv

import wx.lib.scrolledpanel as scrolled
import wx
import mysql.connector
from wx.lib import sized_controls
ID_YES = wx.NewId()
ID_NO = wx.NewId()
from classification.predict import get_label
class CustomDialog(sized_controls.SizedDialog):

    def __init__(self, *args, **kwargs):
        super(CustomDialog, self).__init__(*args, **kwargs)
        pane = self.GetContentsPane()

        static_line = wx.StaticLine(pane, style=wx.LI_HORIZONTAL)
        static_line.SetSizerProps(border=(('all', 0)), expand=True)

        pane_btns = sized_controls.SizedPanel(pane)
        pane_btns.SetSizerType('horizontal')
        pane_btns.SetSizerProps(align='center')

        button_ok = wx.Button(pane_btns, ID_YES, label='Yes')
        button_ok.Bind(wx.EVT_BUTTON, self.on_button)

        button_ok = wx.Button(pane_btns, ID_NO, label='No')
        button_ok.Bind(wx.EVT_BUTTON, self.on_button)

        self.Fit()

    def on_button(self, event):
        if self.IsModal():
            self.EndModal(event.EventObject.Id)
        else:
            self.Close()
class PupilsPanel(scrolled.ScrolledPanel):
    def __init__(self, parent):
        scrolled.ScrolledPanel.__init__(self, parent)

        '''self.scrolled_panel = scrolled.ScrolledPanel(self, -1,
                                                     style=wx.TAB_TRAVERSAL | wx.SUNKEN_BORDER, name="panel1")
        self.scrolled_panel.SetAutoLayout(1)
        self.scrolled_panel.SetupScrolling()'''
        self.SetAutoLayout(1)
        self.SetupScrolling()
        self.frame=parent
        self.mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="Veve831674",
            database="dropoutDatabase"
        )
        self.mycursor = self.mydb.cursor()

        self.mycursor.execute("SELECT * FROM SchoolPupils")
        myresult = self.mycursor.fetchall()
        size=0
        self.btndict={}
        print(myresult)
        for x in myresult:
            size+=1
        if size !=0 :
            height=int(size/4)+size%4
            sizer = wx.GridBagSizer(height+1, 4)
            indexw=0
            indexh=0
            for pupil in myresult :
                if indexw>=4:
                    indexw=0
                    indexh+=1
                sb = wx.StaticBox(self, label="")
                boxsizer = wx.StaticBoxSizer(sb, wx.VERTICAL)
                label=wx.StaticText(self, label=pupil[0])
                stylish_font = wx.Font(wx.FontInfo(15).FaceName("liberation").Bold())
                label.SetFont(stylish_font)
                boxsizer.Add(label, flag=wx.CENTER, border=5)
                boxsizer.Add(wx.StaticLine(self), flag=wx.CENTER, border=40)
                image = wx.Bitmap("imgs/user.png")
                d = wx.StaticBitmap(self, wx.ID_ANY, image)
                boxsizer.Add(d, flag=wx.CENTER, border=5)
                boxsizer.Add(wx.StaticText(self, label="ID : "+str(pupil[2])), flag=wx.Left, border=5)
                boxsizer.Add(wx.StaticText(self, label="GPA : " + str(pupil[1])), flag=wx.Left, border=5)
                boxsizer.Add(wx.StaticText(self, label="Parents' Marital Status  : " + str(pupil[3])), flag=wx.Left, border=5)
                boxsizer.Add(wx.StaticText(self, label="Father Status : " + str(pupil[4])), flag=wx.Left, border=5)
                boxsizer.Add(wx.StaticText(self, label="Mother Status : " + str(pupil[5])), flag=wx.Left, border=5)
                boxsizer.Add(wx.StaticText(self, label="Self Esteem : " + str(pupil[6])), flag=wx.Left, border=5)
                boxsizer.Add(wx.StaticText(self, label="Discipline : " + str(pupil[7])), flag=wx.Left, border=5)
                sb1 = wx.StaticBox(self, label="")
                boxsizer1 = wx.StaticBoxSizer(sb1, wx.VERTICAL)
                status=""
                colortxt=""
                Fatheremp=0
                Motheremp=0
                parentstat=0
                if str(pupil[3]) == "Marriage" :
                    parentstat=1
                else:
                    parentstat=0

                if str(pupil[4]) == "Working" :
                    Fatheremp=1
                else:
                    Fatheremp=0
                if str(pupil[5]) == "Working":
                    Motheremp = 1
                else:
                    Motheremp = 0
                gpa=70
                flag=False
                try:
                    gpa = int(pupil[1])
                except ValueError:
                    # Handle the exception
                    print('Please enter an integer')
                row=[]
                try:

                    x=get_label('clustered-data/clustered_data.csv',[gpa,Fatheremp,Motheremp,parentstat,int(pupil[6]),int(pupil[7])])
                    print(pupil[1], Fatheremp, Motheremp, parentstat, pupil[6], pupil[7])
                    row=[gpa,Fatheremp,Motheremp,parentstat,int(pupil[6]),int(pupil[7])]
                    if x ==0 :
                        status="High Risk"
                        colortxt=wx.Colour(192,22,38)
                    else :
                        status = "Low Risk"
                        colortxt = wx.Colour(9,201,88)


                    labelrisk = wx.StaticText(self, label=status)
                    labelrisk.SetForegroundColour(colortxt)
                    stylish_font1 = wx.Font(wx.FontInfo(10).FaceName("liberation").Bold())
                    labelrisk.SetFont(stylish_font1)

                    boxsizer1.Add(labelrisk, flag=wx.CENTER, border=5)
                except FileNotFoundError:
                    print("Wrong file or file path")
                btn_verify = wx.Button(self, 0, label="Verify")

                btn_verify.Bind(wx.EVT_BUTTON, self.verify_event)
                self.btndict[btn_verify]=row
                boxsizer1.Add(btn_verify,flag=wx.CENTER, border=5)
                boxsizer.Add(boxsizer1, flag=wx.CENTER, border=5)
                sizer.Add(boxsizer, pos=(indexh, indexw), span=(1, 1),
                          flag=wx.EXPAND | wx.TOP | wx.LEFT | wx.RIGHT, border=10)
                indexw+=1
            self.closebut = wx.Button(self, label="Close")
            sizer.Add(self.closebut, pos=(height, 1), span=(1, 1),
                      flag=wx.EXPAND |wx.CENTER, border=10)
            #self.scrolled_panel.SetSizer(sizer)
            self.SetSizer(sizer)
            #sizer.Fit(self.scrolled_panel)

    def verify_event(self, event):
        btn = event.GetEventObject()
        row=self.btndict[btn]
        if len(row) >0 :
            dlg = CustomDialog(None, title='Dropout Verify')
            result = dlg.ShowModal()
            dropoutclass=-1
            if result == ID_YES:
                dropoutclass=1
            elif result == ID_NO:
                dropoutclass=0
            if dropoutclass!=-1:
                row.append(dropoutclass)
                with open('clustered-data/verified_data.csv', 'a') as fd:
                    writer = csv.writer(fd)
                    writer.writerow(row)
                self.btndict[btn]=[]

            dlg.Destroy()


